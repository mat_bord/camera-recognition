*******************
CAMERA MODEL IDENTIFICATION USING CFA
author: Simone Milani (simone.milani@dei.unipd.it) 
date: 30/11/2017
license: This project is released under the GNU Public License.
******************



There are three main files to be used

****
Dataset creation
***

Inside the folder “dataset” you can use the script “scarica.m” to download the images in  the folder

you must have the files at first

*****
Create image patches to be classify
****

createFeatDresden2.m: generates the image patches to be classified by the CNN

In every image it selects a 64x64 patch with significant variance

****
Create the CNN and classify
****

classify_cfa.m: creates the network and use it to classify the patches.

The idea is to take multiple patches in an image and combine the results of their classification in oder to have a high accuracy