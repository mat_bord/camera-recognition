clear all;
close all;
close all hidden;

close all;
clear all;
%define path to useful functions
if ispc
	addpath('func_arc');
else
	addpath('./func_arc');
end;

%name of the camera models to be identified
vet_prefix={ 'Canon_Ixus70' 'Kodak_M1063' 'Nikon_CoolPixS710' 'Casio_EX-Z150' ...
    'FujiFilm_FinePixJ50' 'Nikon_D200' 'Nikon_D70s' };

%parameters to be used
szp=64;  %size of the patches processed by CFA detection
th_val1=0.4;%threshold minimum variance of patch
th_val2=10;  

res=[];  %matrix of feature arrays

%images to be processed by CNN
Img=zeros(szp,szp,3,20);

res=[];
cameramodel='pixel';
results(1,1)="Majority";
results(1,2)="Dempster";
    
for ind = 1:4

        %read images
        I0=imread("Test/pixel2xl/"+cameramodel+ind+".jpg");
        [N0,M0,ch0]=size(I0);  %size of the image

        R=I0(:,:,1);  %R component
        G=I0(:,:,2);  %G component
        B=I0(:,:,3);  %B component

        Rc=ROFdenoise(R,14);  %denoise components
        Gc=ROFdenoise(G,14);
        Bc=ROFdenoise(B,14);

        %create difference image between original and denoised image
        D0=I0;
        D0(:,:,1)=double(R)-Rc;
        D0(:,:,2)=double(G)-Gc;
        D0(:,:,3)=double(B)-Bc;

        %for every image create 10 szbxszb patches to be classified

        cpatch=1;
        %iteration in case there are too many attempts to search a
        %patch
        iteration=0;
        while cpatch <= 20
            %if it reach 100 attempts for searching one patch, go to
            %another photo
            if iteration==100
                break;
            end;

            %select (x,y) randomly inside each image (coordinates of the upper
            %left pixel in the pixel)
            x=randi(M0-szp,1,1);%return an array 1x1 with a casual number from 1 to M0-szp 
            y=randi(N0-szp,1,1);

            %select the patch
            I=I0(y:y+szp-1,x:x+szp-1,:);
            D=D0(y:y+szp-1,x:x+szp-1,:);

            %components for each patch (block)
            rb=R(y:y+szp-1,x:x+szp-1);
            gb=G(y:y+szp-1,x:x+szp-1);
            bb=B(y:y+szp-1,x:x+szp-1);

            %compute variances of the signal in the patch (block)
            vr=var(double(rb(:)));
            vg=var(double(gb(:)));
            vb=var(double(bb(:)));

            %average variance
            avg_v=(vb+vr+vg)/3;%for every patch calculate the variance

            %this operation is required in order to avoid using patches with
            %low or high variance 
            if (avg_v<th_val1 || avg_v>th_val2)
                iteration=iteration+1;
                continue;
            end;
            %if it founds a patch, reset iteration
            iteration=0;
                
            %save the features in the structure Img
            Img(:,:,:,cpatch)=D;

            fprintf('Patch %d complete\n', cpatch);
            cpatch=cpatch+1; %increase counter of the patch
        end;

        load net_0.4_10.mat
        miniBatchSize = 256;
        [label_out,scores] = classify(net,Img,'ExecutionEnvironment','gpu',...
            'MiniBatchSize',miniBatchSize)
        % Dempster
        
        normalized=scores(1,:);
        for c=2:20
            numerator=normalized.*scores(c,:);
            denominator=sum(numerator);
            normalized=numerator/denominator;    
        end

        disp('normalized matrix :');
        disp(normalized);
        [val, idx] = max(normalized);

        A = mode(double(label_out)); %most frequently occurring value in each column in  A. Label_out has only one column so return only one value
        sorted = sort(label_out);%sort the array from lower to higher
        fprintf('Value of majority: %d ==> %s\n',A,vet_prefix{A});
        fprintf('Value of Dempster: %d ==> %s\n',idx,vet_prefix{idx});
        %good results with the image Nikon_D70s_1_23112.JPG

        results(ind+1,1)=A;
        results(ind+1,2)=idx;
end;

disp(results);