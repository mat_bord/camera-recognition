function[ind]=select_patches(Img,th_valmin, th_valmax)
n=size(Img,4);

flg=zeros(1,n);
    
for nf=1:n %n=number of patches
	rgberr=Img(:,:,:,nf);
	comp=rgberr(:,:,1);
	vr=var(comp(:));
	comp=rgberr(:,:,2);
	vg=var(comp(:));
	comp=rgberr(:,:,3);
	vb=var(comp(:));
        
	avg_v=(vr+vb+vg)/3; %for every patch calculate the variance and select only the good patches
        
	if (avg_v>th_valmin && avg_v<th_valmax ) %find only interesting patches and then return them
        flg(nf)=1;
	end;
        
end;
    
ind=find(flg>0);
