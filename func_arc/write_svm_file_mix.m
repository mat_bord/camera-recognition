function[]=write_svm_file_mix(mat1,out1,str)

sz=size(mat1);

fp=fopen(str,'w');

for i=1:sz(1)
    if (out1(i)>0)
        fprintf(fp,'+1 ');
    else
        fprintf(fp,'-1 ');
    end;
    for j=1:sz(2)
		fprintf(fp,'%d:%f ',j,mat1(i,j));
	end;
	fprintf(fp,' # data %d \n',i);
end;

fclose(fp);


