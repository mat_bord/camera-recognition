%function[]=create_svm_file(fff0,fff1,cl_vet,str);
%
%author: Simone Milani (simone.milani@dei.unipd.it) 
%date: 30/09/2014
%license: This project is released under the GNU Public License.
%
%Creates an SVM classifier using svm_light package
%the classifier is stored in the model file str.
%the classifier selects whether the feature array belongs to a class
%or not (binary classifier)
%
%Usage:
%
%input:
%str:    model file for SVM light
%cl_vet: array of classes to be used in the recall/precision computation
%fff0:   array of features for class +1
%fff1:   array of features for class -1
%
%output: none


function[]=create_svm_file(fff0,fff1,cl_vet,str);

%finds whetehr you are using linux/unix or windows
flag_win=double(isunix==0);

if isunix
	addpath('./func_arc');
else
	addpath('func_arc');
end;

%number of feature vector for class +1
N0=size(fff0,1);
%number of feature vector for class -1
N1=size(fff1,1);

%optimze with cross validation
%number of training arrays in the cross-validation
Nt0=floor(N0*.8);
Nt1=floor(N1*.8);
%number of testing arrays in the cross-validation
Nv0=N0-Nt0;
Nv1=N1-Nt1;

%set the classes for precision/recall computation
vnum=unique(cl_vet);

best_perc=0;
best_nv=100000;
best_g=0;
best_perc_vet=[];
for trial=1:10
    
    ind0=randperm(N0);
    ind1=randperm(N1);
    
    Nt=min(Nt0,Nt1);
    
    %trainig set for cross validation
    mt0=fff0(ind0(1:Nt),:);
    mt1=fff1(ind1(1:Nt),:);
    %etsting set
    mv0=fff0(ind0(Nt+1:N0),:);
    mv1=fff1(ind1(Nt+1:N1),:);
    %classes for precision computation
    cl1_vet=cl_vet(ind1(Nt+1:N1));
    
    %write SVM light files
    write_svm_file(mt0,mt1,'train_rn.dat');
    write_svm_file(mv0,mv1,'test_rn.dat');
    
    %iterate on g parameters for RBF kernel
    for g=.05:.05:10
        
        %train detector
        if (flag_win==1)
        
            system(sprintf('func_arc\\svm_learn.exe -v 0 -c %f -x 1  train_rn.dat model',g));
        
            system(sprintf('func_arc\\svm_classify.exe -v 0  test_rn.dat model test_pred.txt ')); 
        
    
        else
        
            system(sprintf('./func_arc/svm_learn -v 0 -c %f -x 1  train_rn.dat model ',g));
         
            system(sprintf('./func_arc/svm_classify -v 0  test_rn.dat model test_pred.txt ')); 
         
        end;
    
        %load outputs of classifier
        fpt=fopen('test_pred.txt','r');
        res_val=fscanf(fpt,'%f');
        fclose(fpt);
        %compute number of SV
        fpt=fopen('model','r');
        arr_data=fscanf(fpt,'%s',100);
        fclose(fpt);
        i0=strfind(arr_data,'#highestfeatureindex')+20;
        i2=strfind(arr_data,'#numberofsupportvectorsplus1')-1;
        i1=strfind(arr_data,'#numberoftrainingdocuments')+26;
        nsupp=str2num(arr_data(i1:i2));
        ntot=str2num(arr_data(i0:i1-27));
    
        %separate outputs for class
        res0=res_val(1:size(mv0,1),:);
        res1=res_val(size(mv0,1)+1:size(mv0,1)+size(mv1,1),:);
        
	%compute the precision for each class
        %This is needed in a multiclass classifier made of binary ones since
        %it is possible that the classifier performs very badly on the feature 
        %arrays of a single class which is mixed with others in one of the two 
        %classes, e.g., +1: camera 1 | -1: all the other cameras

	%perc_vet contains the precision of the classifier for each class
        perc_vet=[];
        perc=length(find(res0(:)>0))/length(res0);
        perc_vet=[ perc_vet perc ];
        for nc=vnum'
            rest=res1(find(cl1_vet==nc));
            perc=length(find(rest(:)<0))/length(rest);
            perc_vet=[ perc_vet perc ];
        end;
        
	%final cost function weights minimum with average precision
        perc_tot=.9*min(perc_vet)+.1*mean(perc_vet);
        %perc_tot=min(perc_vet);
        
	%find the best classifier: note that if precision does not improve
        %a reduction in the number of SV may lead to a better classifier.
        %EXperimental results show that this leads to a more robust classifier
        if (perc_tot>best_perc)|((perc_tot>(best_perc*0.999))&(nsupp<best_nv))
            system(sprintf('cp -f model %s',str)); 
            best_perc=perc_tot;
            best_perc_vet=perc_vet;
            best_g=g;
            best_nv=nsupp;
	    %partial stats
            disp([trial g perc_tot best_g best_perc]);
        end;
        
    end;
    
    %final stats
    disp([trial best_g best_perc best_perc_vet]);
    
end;

 delete model;
 delete test_rn.dat;
 delete train_rn.dat;
 delete test_pred.txt;
