function[mse4_vet,mse6_vet,mse8_vet]=int_feat_diff(I,method,app,qf)
I=double(I);
[N,M,ch]=size(I);
R=I(:,:,1);
G=I(:,:,2);
B=I(:,:,3);

Rc=ROFdenoise(R,14);
Gc=ROFdenoise(G,14);
Bc=ROFdenoise(B,14);

Ic=I;
Ic(:,:,1)=Rc;
Ic(:,:,2)=Gc;
Ic(:,:,3)=Bc;

if (qf>0)
    imwrite(Ic/255,'ciccio.jpg','Quality',qf);
    Ic=double(imread('ciccio.jpg'));
end;

Ic=I-Ic;
R=Ic(:,:,1);
G=Ic(:,:,2);
B=Ic(:,:,3);

[X,Y]=meshgrid(1:M,1:N);
Rm=zeros(N,M);
Gm=zeros(N,M);
Bm=zeros(N,M);

flag_fig=0;


% mat_freq=[ 0 .1 .6 1 ; 0 .15 .6 1 ; 0 .2 .6 1 ; 0 .25 .6 1 ; 0 .3 .6 1 ; ...
%     0 .35  .6 1 ; 0 .4  .6 1 ; 0 .45  .6 1 ; 0 .5  .6 1 ; ...
%     0 .5  .65 1 ; 0 .5  .7 1 ; 0 .5  .75 1 ; 0 .5  .8 1 ; 0 .5  .85 1 ; 0 .5  .9 1];
% 
% b4=firpm(4,mat_freq(method,:),[1 1 0 0]);
% b4=b4/sqrt(mean(b4.^2));
% b6=firpm(6,mat_freq(method,:),[1 1 0 0]);
% b6=b6/sqrt(mean(b6.^2));
% b8=firpm(8,mat_freq(method,:),[1 1 0 0]);
% b8=b8/sqrt(mean(b8.^2));

load filter_coeff_arc;
b4=cell2mat(filter_coeff_mat(method,1));
b6=cell2mat(filter_coeff_mat(method,2));
b8=cell2mat(filter_coeff_mat(method,3));

switch app
    
    case 1

    Rm(1:2:N,1:2:M)=1;
	Bm(2:2:N,2:2:M)=1;
	Gm(1:2:N,2:2:M)=1;
	Gm(2:2:N,1:2:M)=1;

    case 2
        
    Bm(1:2:N,1:2:M)=1;
	Rm(2:2:N,2:2:M)=1;
	Gm(1:2:N,2:2:M)=1;
	Gm(2:2:N,1:2:M)=1;

    case 3
    
    Gm(1:2:N,1:2:M)=1;
	Gm(2:2:N,2:2:M)=1;
	Rm(1:2:N,2:2:M)=1;
	Bm(2:2:N,1:2:M)=1;
        
    case 4
        
    Gm(1:2:N,1:2:M)=1;
	Gm(2:2:N,2:2:M)=1;
	Bm(1:2:N,2:2:M)=1;
	Rm(2:2:N,1:2:M)=1;
end;    

Go=G;
Bo=B;
Ro=R;



tot=N*M;

mse4_vet=[];
mse6_vet=[];
mse8_vet=[];


for col=1:3

    if (col==1)
        Mo=Go;
        Mm=Gm;
        Mr=G;
    elseif (col==2)
        Mo=Ro;
        Mm=Rm;
        Mr=R;
    else
        Mo=Bo;
        Mm=Bm; 
        Mr=B;
    end;
    
    ind=find(Mm(:)==0);
    Mo(ind)=0;
    
    pwr=mean(Mr(:).^2);
    
    Mt=filter(b4,1,Mo);
    Mo=filter(b4,1,Mt');
    
    Mo=Mo';
    Mo=Mo(3:N,3:M);
    pwo=mean(Mo(:).^2);
    Mo=Mo*sqrt(pwr/pwo);
    
    if (flag_fig == 1)
    figure(1);
    subplot(2,3,col);
    imshow(Mr/255);
    subplot(2,3,col+3);
    imshow(Mo/255);
    end;
    
    mMm=Mm(1:N-2,1:M-2);
    ind=find(mMm(:)==0);
    diff=Mr(1:N-2,1:M-2)-Mo;
    mse4=sqrt(mean(diff(ind).^2));
    
    if (col==1)
        Mo=Go;
        Mm=Gm;
        Mr=G;
    elseif (col==2)
        Mo=Ro;
        Mm=Rm;
        Mr=R;
    else
        Mo=Bo;
        Mm=Bm; 
        Mr=B;
    end;
    
    ind=find(Mm(:)==0);
    Mo(ind)=0;
    
    Mt=filter(b6,1,Mo);
    Mo=filter(b6,1,Mt');
    
    Mo=Mo';
    Mo=Mo(4:N,4:M);
    pwo=mean(Mo(:).^2);
    Mo=Mo*sqrt(pwr/pwo);
    
    if (flag_fig == 1)
    figure(2);
    subplot(2,3,col);
    imshow(Mr/255);
    subplot(2,3,col+3);
    imshow(Mo/255);
    end;
    
    mMm=Mm(1:N-3,1:M-3);
    ind=find(mMm(:)==0);
    diff=Mr(1:N-3,1:M-3)-Mo;
    mse6=sqrt(mean(diff(ind).^2));
    
    if (col==1)
        Mo=Go;
        Mm=Gm;
        Mr=G;
    elseif (col==2)
        Mo=Ro;
        Mm=Rm;
        Mr=R;
    else
        Mo=Bo;
        Mm=Bm; 
        Mr=B;
    end;
    
    ind=find(Mm(:)==0);
    Mo(ind)=0;
    
    Mt=filter(b8,1,Mo);
    Mo=filter(b8,1,Mt');
    
    Mo=Mo';
    Mo=Mo(5:N,5:M);
    pwo=mean(Mo(:).^2);
    Mo=Mo*sqrt(pwr/pwo);
    
    if (flag_fig == 1)
    figure(3);
    subplot(2,3,col);
    imshow(Mr/255);
    subplot(2,3,col+3);
    imshow(Mo/255);
    end;
    
    mMm=Mm(1:N-4,1:M-4);
    ind=find(mMm(:)==0);
    diff=Mr(1:N-4,1:M-4)-Mo;
    mse8=sqrt(mean(diff(ind).^2));
    
    mse4_vet=[ mse4_vet ; mse4 ];
    mse6_vet=[ mse6_vet ; mse6 ];
    mse8_vet=[ mse8_vet ; mse8 ];
    

end;

