%[res_val]=class_svm_arr(fff0,str)
%
%author: Simone Milani (simone.milani@dei.unipd.it) 
%date: 30/09/2014
%license: This project is released under the GNU Public License.
%
%Classify features in the input array
%
%Usage:
%
%input:
%str:    model file for SVM light classifier
%fff0:   array of features 
%
%output: 
%res_val: outputs of the classifier (real numbers)

function[res_val]=class_svm_arr(fff0,str)

%finds whetehr you are using linux/unix or windows
flag_win=double(isunix==0);

if isunix
	addpath('./func_arc');
else
	addpath('func_arc');
end;

write_svm_file(fff0,[],'test_final.dat');
if (flag_win==1)
	system(sprintf('func_arc\\svm_classify.exe -v 0  test_final.dat %s test_pred.txt ',str)); 
else
    system(sprintf('./func_arc/svm_classify -v 0  test_final.dat %s test_pred.txt ',str)); 
end;
    
fpt=fopen('test_pred.txt','r');
res_val=fscanf(fpt,'%f');
fclose(fpt);

delete test_pred.txt
delete test_final.dat
