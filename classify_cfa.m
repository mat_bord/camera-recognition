%Description:
%This script is supposed to classify the patches generated for the camera
%model identification

%author: Simone Milani (simone.milani@dei.unipd.it) 
%date: 30/11/2017
%license: This project is released under the GNU Public License.
%

clear all;
close all;
close all hidden;

close all;
clear all;
%define path to useful functions
if ispc
	addpath('func_arc');
else
	addpath('./func_arc');
end;

%name of the camera models to be identified
vet_prefix={ 'Canon_Ixus70' 'Kodak_M1063' 'Nikon_CoolPixS710' 'Casio_EX-Z150' ...
    'FujiFilm_FinePixJ50' 'Nikon_D200' 'Nikon_D70s' }

%directory where dataset is to be found
%str_dir='/dataset/DresdenDataset/'; %not used

%parameters to be used
Nclasses=7;
%0.7-10000 5760 samples <--net_total 97% saved
%0.7-2.5 3660 samples
%0.2-5 6960 samples_____ 0.3-5 6820 samples____ 0.5-5 6100 samples____
%0.4-5 6480 samples_____0.4-10 6860 samples____0.4-15 6940 samples
%-->0.3-10 7200 samples good number <--97.63% Tra 10 e 15 c'è troppa poca
%differrenza di samples quindi l'unica soluzione sarebbe di alzare lo 0.3)
%-->0.4-10 6860 samples <--97.87%
th_val1=0.35;
th_val2=20;
%organize the generated data to fit to MATLAB CNN functions
label_train=[];
ntot_train=0;
label_test=[];
ntot_test=0;
%loop on classes and write the label for every good patch
for c=1:Nclasses
    
    %prepare training data
    
    %load files
    eval(sprintf('load feat_cam_trainC%d res vet_tot_files Img',c));
    %n=size(Img,4);
    
    %select good patches
    ind=select_patches(Img,th_val1,th_val2);
    
    n=length(ind);
    
    ntot_train=ntot_train+n;  %count the total number of training patches
    label_train=[ label_train (ones(1,n)*c) ];  %select the labels
    
    %prepare test data
    
    eval(sprintf('load feat_cam_testC%d res vet_tot_files Img',c));
    
    %select images
    
    ind=select_patches(Img,th_val1,th_val2);
    
    n=length(ind);
    
    ntot_test=ntot_test+n;  %count the total number
    label_test=[ label_test (ones(1,n)*c) ];  %select labels
end;

%create two arrays of images to be used with MATLAB Neural Toolbox
Itrain=zeros(64,64,3,ntot_train);
Itest=zeros(64,64,3,ntot_test);
Ntrain=ntot_train;
Ntest=ntot_test;


ntot_train=0;
ntot_test=0;
%select only good patches for every camera and save them in an array called
%Itrain/Itest
for c=1:Nclasses
    
    %prepare training data
    
    %load data
    eval(sprintf('load feat_cam_trainC%d res vet_tot_files Img',c));
    
    %return only good patches
    ind=select_patches(Img,th_val1,th_val2);%
    
    n=length(ind);
    
    %save patches in the struct Itrain
    Itrain(:,:,:,ntot_train+1:ntot_train+n)=Img(:,:,:,ind);
    ntot_train=ntot_train+n;
    
    
    %prepare testing (evaluation) data
    
    %load data and variables
    eval(sprintf('load feat_cam_testC%d res vet_tot_files Img',c));

    
    ind=select_patches(Img,th_val1,th_val2);%
    
    n=length(ind);
    
    %save the patches of the Test set in Itest
    Itest(:,:,:,ntot_test+1:ntot_test+n)=Img(:,:,:,ind);
    ntot_test=ntot_test+n;
end;


Ten_train=Itrain;
Ten_test=Itest;


labels=categorical(label_train);

label_t=categorical(label_test);

%%
%This part creates the CNN and do the classification
%%

%parameters for CNN
miniBatchSize = 256;
numValidationsPerEpoch = 2;
validationFrequency = floor(size(Itrain,4)/miniBatchSize/numValidationsPerEpoch);

%options for CNN
%for more information go to the link 
%https://it.mathworks.com/help/nnet/ref/trainingoptions.html?requestedDomain=www.mathworks.com
options = trainingOptions('sgdm',...        % Use the stochastic gradient descent with momentum (SGDM) optimizer.
    'ExecutionEnvironment','gpu',...        %use the gpu as Hardware resource for training network
     'LearnRateSchedule','piecewise',...    %Option for dropping the learning rate during training:The software updates the learning rate every certain number of epochs by multiplying with a certain factor.
     'InitialLearnRate',.005,...            %Initial learning rate
    'LearnRateDropFactor',0.2,...           %Factor for dropping the learning rate
    'LearnRateDropPeriod',20,...            %Number of epochs for dropping the learning rate (Epoch : It simply represents one iteration over the entire dataset)
    'MaxEpochs',20,...                      % Maximum number of epochs
    'ValidationData',{Ten_test,label_t'},...% Data to use for validation during training
    'ValidationFrequency',30,...            %Frequency of network validation
    'VerboseFrequency',30,...               % Frequency of verbose printing
    'Verbose',true,...                      %Indicator to display training progress information
    'Plots','training-progress');


%structure of the CNN (list of layers)
%https://it.mathworks.com/help/deeplearning/ref/nnet.cnn.layer.convolution2dlayer.html#d120e21739

layers = [
    imageInputLayer([64 64 3])

    convolution2dLayer(7,16,'Padding',3)
    batchNormalizationLayer
    reluLayer

    
    maxPooling2dLayer(2,'Stride',2)

    convolution2dLayer(7,32,'Padding',3)
    batchNormalizationLayer
    reluLayer

    maxPooling2dLayer(2,'Stride',2)

    convolution2dLayer(7,64,'Padding',3)
    batchNormalizationLayer
    reluLayer
    %--------
    %dropoutLayer
    %-----------

   %works also if I remove them
%     maxPooling2dLayer(2,'Stride',2)
% 
%     convolution2dLayer(7,128,'Padding',3)
%     batchNormalizationLayer
%     reluLayer
    
%     maxPooling2dLayer(2,'Stride',2)
% 
%     convolution2dLayer(7,256,'Padding',3)
%     batchNormalizationLayer
%     reluLayer

    
    fullyConnectedLayer(Nclasses)
    softmaxLayer %probability that the picture is taken with that camera
    classificationLayer];
%%
%end of parameters for CNN
%%

%%
%train and test the network
%%

 
rng('default') % For reproducibility
net = trainNetwork(Ten_train,labels',layers,options);  %train
%%%%
eval(sprintf('save net net'));%save the net
%%%%
label_out = classify(net,Ten_test,'ExecutionEnvironment','gpu',...
    'MiniBatchSize',miniBatchSize);  %classify evaluatio test

accuracy = sum(label_out(:) == label_t(:))/numel(label_t)  %average accuracy

%Confusion matrix
conf_mat=zeros(Nclasses,Nclasses);  
for c1=1:Nclasses
    ind1=find(double(label_t(:))==c1);  %select values that belong to c1 classes
    for c2=1:Nclasses
        ind2=find(double(label_out(:))==c2);  %elements classified as c2
        ind=intersect(ind1,ind2);  %c1 elements classified as c2
        conf_mat(c1,c2)=length(ind);  %save the number
    end;
end;
%normalize the numbers into percentages
conf_mat=conf_mat./(sum(conf_mat,2)*ones(1,Nclasses))*100;
disp(conf_mat);  %visualize confusion matrix

%on each columns how many photos are assigned to these type of camera
%during the classification

%on each row how have been classified each block of photos belonging to the
%camera labelled on the rows. The sum of all percentage is 100%

%The percentage express how many photos  are assigned to the camera model
%labelled on the columns. So on the raw are labelled the real camera and on
%the column the model camera assigned by the ccn.

%Epoch: the first block of training is given
%mini-batch accuracy: the accuracy of the particular mini-batch at the given iteration.
%
